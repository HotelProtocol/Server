package server

//go:generate mockgen -destination=./mock_server/mock_guest.go -package=mock_server gitlab.com/prosign/server Guest

// Guest represents someone connected to our server
type Guest interface {
	ID() string
	GetMessage() (string, []byte, error)
	SendMessage(string, []byte) error
	SendError(header string, err error) error
}
