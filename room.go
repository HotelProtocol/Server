package server

import (
	"errors"
)

// Room has guests talking to eachother
type Room struct {
	guests      []Guest
	displayName string
}

func newRoom(displayName string, guest Guest) (*Room, error) {
	if guest == nil {
		return nil, errors.New("Can't create room with no connections")
	}
	room := &Room{
		guests:      []Guest{guest},
		displayName: displayName,
	}
	return room, nil
}

// CheckGuestIntoRoom adds a guest to the room
func (room *Room) CheckGuestIntoRoom(guest Guest) error {
	if guest == nil {
		return errors.New("Guest can't be nil")
	}
	room.guests = append(room.guests, guest)
	return nil
}

// RemoveGuest from the room
func (room *Room) RemoveGuest(guest Guest) (int, error) {
	if guest == nil {
		return -1, errors.New("Can't remove a guest that doesn't exist")
	}
	index := -1
	for i := range room.guests {
		if room.guests[i] == guest {
			index = i
			break
		}
	}

	if index == -1 {
		return -1, errors.New("Guest does not exist in room")
	}
	room.guests = append(room.guests[:index], room.guests[index+1:]...)
	return len(room.guests), nil
}

// UpdateAllOtherGuests sends an update to all guests in the room except the one passed
func (room *Room) UpdateAllOtherGuests(guest Guest, body []byte) error {
	if guest == nil {
		return errors.New("Guest can't be nil")
	}

	for i := range room.guests {
		if room.guests[i] != guest {
			err := room.guests[i].SendMessage("room.update", body)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
