// +build unit

package server_test

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	. "gitlab.com/prosign/server"
	mock_server "gitlab.com/prosign/server/mock_server"
)

func TestGetsRoomIdOnCreate(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockGuest := mock_server.NewMockGuest(ctrl)

	roomService := NewRoomService(NewTerminalLogger(false))
	response, err := roomService.Forward("create", nil, mockGuest)

	if err != nil {
		t.Error(err.Error())
	}

	regex := regexp.MustCompile(`....................................$`)

	if regex.MatchString(string(response)) == false {
		t.Errorf("Unexpected response from server: %s", response)
	}
}

func TestGetsErrorConnectingToRoomThatDoesntExist(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockGuest := mock_server.NewMockGuest(ctrl)

	roomService := NewRoomService(NewTerminalLogger(false))
	response, err := roomService.Forward("join", []byte("abc"), mockGuest)

	if err == nil {
		t.Error(errors.New("Expected error"))
	}

	if len(response) > 0 {
		t.Error(errors.New("Expected empty response, got: " + string(response)))
	}

	if err.Error() != "Room does not exist" {
		t.Error("Unexpected error message: " + err.Error())
	}
}

func TestGuestsCanConnectToRoomAndTalkToEachother(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockGuestOne := mock_server.NewMockGuest(ctrl)

	mockGuestTwo := mock_server.NewMockGuest(ctrl)
	mockGuestTwo.EXPECT().SendMessage("room.update", []byte("from one")).Times(1)

	roomService := NewRoomService(NewTerminalLogger(false))

	// Create Room
	roomID, err := roomService.Forward("create", []byte("abc"), mockGuestOne)
	if err != nil {
		t.Error(err)
	}

	// 2nd Join room
	response, err := roomService.Forward("join", roomID, mockGuestTwo)

	if err != nil {
		t.Error(err)
	}

	if string(response) != "connected" {
		t.Errorf("Unexpected response: %s", response)
	}

	// First sends update
	_, err = roomService.Forward("update", []byte("from one"), mockGuestOne)
	if err != nil {
		t.Error(err)
	}

	mockGuestOne.EXPECT().SendMessage("room.update", []byte("from two")).Times(1)
	_, err = roomService.Forward("update", []byte("from two"), mockGuestTwo)
	if err != nil {
		t.Error(err)
	}

	mockGuestOne.EXPECT().SendMessage("room.update", []byte("two again")).Times(1)
	_, err = roomService.Forward("update", []byte("two again"), mockGuestTwo)
	if err != nil {
		t.Error(err)
	}

}

func TestGetSucessfulResponseWhenTryingToLeaveRoom(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockGuestOne := mock_server.NewMockGuest(ctrl)

	mockGuestTwo := mock_server.NewMockGuest(ctrl)

	roomService := NewRoomService(NewTerminalLogger(false))

	// Create Room
	roomID, _ := roomService.Forward("create", []byte("abc"), mockGuestOne)

	// 2nd Joins room
	roomService.Forward("join", roomID, mockGuestTwo)

	response, err := roomService.Forward("leave", nil, mockGuestOne)
	if err != nil {
		t.Error(err)
	}

	if string(response) != "success" {
		t.Errorf("Unexpected response: %s", response)
	}

	response, err = roomService.Forward("leave", nil, mockGuestTwo)
	if err != nil {
		t.Error(err)
	}

	if string(response) != "success" {
		t.Errorf("Unexpected response: %s", response)
	}

}

func TestIncomingUpdateMiddlewareModifiesMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockGuestOne := mock_server.NewMockGuest(ctrl)
	mockGuestTwo := mock_server.NewMockGuest(ctrl)
	roomService := NewRoomService(NewTerminalLogger(false))

	roomService.InterceptIncomingUpdate(func(body []byte, guest Guest) ([]byte, error) {
		return append(body, []byte(" - a test signature")...), nil
	})

	roomID, _ := roomService.Forward("create", []byte("abc"), mockGuestOne)
	roomService.Forward("join", roomID, mockGuestTwo)

	mockGuestTwo.EXPECT().SendMessage("room.update", []byte("abc - a test signature")).Times(1)
	roomService.Forward("update", []byte("abc"), mockGuestOne)
}

func TestUpdateIsNotPropegatedIfUpdateIsEmpty(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockGuestOne := mock_server.NewMockGuest(ctrl)
	mockGuestTwo := mock_server.NewMockGuest(ctrl)

	roomService := NewRoomService(NewTerminalLogger(false))
	roomID, _ := roomService.Forward("create", []byte("abc"), mockGuestOne)
	roomService.Forward("join", roomID, mockGuestTwo)

	roomService.Forward("update", []byte(""), mockGuestOne)
	roomService.Forward("update", nil, mockGuestOne)
}

func TestListsRoomsThatArePublic(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockGuestOne := mock_server.NewMockGuest(ctrl)
	mockGuestTwo := mock_server.NewMockGuest(ctrl)

	roomService := NewRoomService(NewTerminalLogger(false))
	abcID, _ := roomService.Forward("create", []byte("abc"), mockGuestOne)
	xyzID, _ := roomService.Forward("create", []byte("xyz"), mockGuestTwo)

	// A private room
	roomService.Forward("create", nil, mockGuestTwo)

	message, err := roomService.Forward("list", nil, mockGuestOne)

	if err != nil {
		t.Error(err)
	}

	messageString := string(message)
	if !strings.Contains(messageString, fmt.Sprintf("abc:%s", abcID)) || !strings.Contains(messageString, fmt.Sprintf("xyz:%s", xyzID)) {
		t.Errorf("unexpected room listing (%s)", message)
	}

}
