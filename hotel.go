package server

import (
	"bytes"
	"errors"
	"fmt"

	uuid "github.com/satori/go.uuid"
)

// Hotel is a collection of rooms
type Hotel struct {
	rooms           map[string]*Room
	guestsCheckedIn map[Guest]string
}

// NewHotel builds us a new hotel with no rooms
func NewHotel() *Hotel {
	hotel := &Hotel{
		rooms:           make(map[string]*Room, 0),
		guestsCheckedIn: make(map[Guest]string),
	}
	return hotel
}

func (hotel Hotel) listRooms() string {
	var buffer bytes.Buffer

	for roomID, room := range hotel.rooms {
		if room.displayName != "" {
			buffer.WriteString(fmt.Sprintf("%s:%s;", room.displayName, roomID))
		}
	}

	return buffer.String()
}

// CreateRoom Builds a room and adds it to the hotel
func (hotel *Hotel) CreateRoom(displayName string, guest Guest) (*Room, string, error) {
	if guest == nil {
		return nil, "", errors.New("Can't create a room without a connection")
	}

	room, err := newRoom(displayName, guest)
	if err != nil {
		return nil, "", err
	}

	key := uuid.Must(uuid.NewV4())
	hotel.rooms[key.String()] = room
	hotel.assignGuestRoom(guest, key.String())

	return room, key.String(), nil
}

func (hotel *Hotel) assignGuestRoom(guest Guest, id string) {
	hotel.guestsCheckedIn[guest] = id
}

// CheckGuestIntoRoom adds a guest to the room
func (hotel *Hotel) CheckGuestIntoRoom(id string, guest Guest) error {
	if id == "" {
		return errors.New("Need a room id to check guest into")
	}

	if guest == nil {
		return errors.New("Guest can't be nil")
	}

	if room, ok := hotel.rooms[id]; ok {
		err := room.CheckGuestIntoRoom(guest)
		if err != nil {
			return err
		}
		hotel.assignGuestRoom(guest, id)
	} else {
		return errors.New("Room does not exist")
	}

	return nil
}

// UpdateRoom sends an update to all guests except the one passed in
func (hotel Hotel) UpdateRoom(guest Guest, body []byte) error {

	if roomID, inRoom := hotel.guestsCheckedIn[guest]; inRoom {
		if room, ok := hotel.rooms[roomID]; ok {
			err := room.UpdateAllOtherGuests(guest, body)
			if err != nil {
				return err
			}
		} else {
			return errors.New("Room does not exist")
		}
		return nil
	}
	return errors.New("Can't update a room if guest isn't in one")
}

// CheckGuestOutOfHotel removes the guest from any room they where apart of
func (hotel *Hotel) CheckGuestOutOfHotel(guest Guest) error {
	if guest == nil {
		return errors.New("Can not checkout a guest that doesn't exist")
	}

	if roomID, inRoom := hotel.guestsCheckedIn[guest]; inRoom {

		if room, ok := hotel.rooms[roomID]; ok {
			remainingGuests, err := room.RemoveGuest(guest)
			if err != nil {
				return err
			}
			if remainingGuests == 0 {
				delete(hotel.rooms, roomID)
			}
		} else {
			return errors.New("Room the guest is in does not exist")
		}

		delete(hotel.guestsCheckedIn, guest)
	}
	return nil

}

// NumberOfRooms determines how many rooms have people at the moment
func (hotel Hotel) NumberOfRooms() int {
	return len(hotel.rooms)
}
