package server

type medium interface {
	Read(b []byte) (n int, err error)
	Write(b []byte) (n int, err error)
}
