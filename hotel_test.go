// +build unit

package server_test

import (
	"testing"

	"github.com/golang/mock/gomock"
	. "gitlab.com/prosign/server"
	mock_server "gitlab.com/prosign/server/mock_server"
)

func TestHotelDeletesRoomsAsTheyBecomeEmpty(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockGuestOne := mock_server.NewMockGuest(ctrl)

	mockGuestTwo := mock_server.NewMockGuest(ctrl)

	roomService := NewHotel()

	_, roomID, err := roomService.CreateRoom("anything", mockGuestOne)
	if err != nil {
		t.Error(err)
	}

	err = roomService.CheckGuestIntoRoom(roomID, mockGuestTwo)
	if err != nil {
		t.Error(err)
	}

	if err = roomService.CheckGuestOutOfHotel(mockGuestOne); err != nil {
		t.Error(err)
	}

	if err = roomService.CheckGuestOutOfHotel(mockGuestTwo); err != nil {
		t.Error(err)
	}

	if roomService.NumberOfRooms() != 0 {
		t.Errorf("Expected no rooms to exist, instead %d do", roomService.NumberOfRooms())
	}

}
